use func
use const
implicit none

real(mp), dimension(n,n) :: ma, mal, mE
real(mp), dimension(n,n) :: X
real(mp), dimension(n) :: lam1, lams
real(mp) :: t, mu
integer :: i, k

mE = 0

do i = 1, n
    do k = i+1, n
        ma(i,k) = sqrt(a*a+(i+k)**2)
        ma(k,i) = sqrt(a*a+(i+k)**2)
    enddo
    ma(i,i) = sqrt(a*a+2*i*i)+1.0/(d+e*i*i)
    mE(i,i) = 1
enddo

do i = 1, n
    print*, ma(i,:)
enddo
print*,' '

x(1,:) = lambda(ma)
lam1 = x(1,:)

print*, lam1

mal = ma - lam1(1) * mE

x(1,:) = lambda(mal)
x(2,:) = 1
lams = x(1,:)/x(1,1)*(x(1,1)+lam1(1))

print*, lams

!Обратный степенной метод
t = lam1(1)
    t=t+1
    x(1,:)=1
    call lambdarev0(ma,x(1,:),mu,t,mE)
    print*,'l', t
    print*, "x",x(1,:)
    print*,' '
lam1 = x(1,:)

do i = 2, n
    x(i,:) = 0
    x(i,i) = 1
    x(i,:) = ort(x(:i-1,:),x(i,:),i-1)
    x(i,:) = x(i,:) / x(i,1)
    t=t+eps
    call lambdarev(ma,x(i,:),mu,t,mE)
    print*,'l', t
    print*, "x",x(i,:)
    print*,' '
enddo
end
